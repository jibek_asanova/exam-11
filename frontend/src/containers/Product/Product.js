import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteProduct, fetchProduct} from "../../store/actions/productsActions";
import {Button, Card, CardContent, CardHeader, CardMedia, Grid, makeStyles, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import imageNotAvailable from "../../assets/images/not_available.png";
import {useLocation} from "react-router-dom";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})
const Product = ({match, history}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const product = useSelector(state => state.products.product);
    const user = useSelector(state => state.users.user);
    const query = useLocation().search

    useEffect(() => {
        dispatch(fetchProduct(match.params.id));
    }, [dispatch, match.params.id]);

    const removeProduct = () => {
        dispatch(deleteProduct(match.params.id, query));
        history.replace('/');
    };


    let cardImage = imageNotAvailable;

    return product && (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={product.title}/>
                {product.image ? <CardMedia
                    image={apiURL + '/' + product.image}
                    title={product.title}
                    className={classes.media}
                /> : <CardMedia
                    image={cardImage}
                    title={product.title}
                    className={classes.media}
                />}
                <CardContent>
                    <Typography variant="subtitle1">
                        Price: {product.price} KGS
                    </Typography>
                    <Typography variant="subtitle1">
                        Description: {product.description}
                    </Typography>
                    <Typography variant="subtitle1">
                        Category: {product.category.title}
                    </Typography>
                    <Typography variant="subtitle1">
                        Owner: {product.user.displayName}
                    </Typography>
                    <Typography variant="subtitle1">
                        Phone Number: {product.user.phoneNumber}
                    </Typography>
                    {product && user && (product.user._id === user._id) && (
                        <Button onClick={removeProduct}>Delete</Button>
                    )}
                </CardContent>
            </Card>
        </Grid>

    );
};

export default Product;