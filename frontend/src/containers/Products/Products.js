import React, {useEffect} from 'react';
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts} from "../../store/actions/productsActions";
import ProductItem from "../../components/ProductItem/ProductItem";
import {useLocation} from "react-router-dom";
import CategoriesMenu from "../../components/UI/AppToolbar/Menu/CategoriesMenu";

const Products = () => {
  const dispatch = useDispatch();
  const products = useSelector(state => state.products.products);
  const fetchLoading = useSelector(state => state.products.fetchLoading);
  const query = useLocation().search;

  useEffect(() => {
    dispatch(fetchProducts(query));
  }, [dispatch, query]);


  return (
      <Grid container direction="column" spacing={2}>
        <Grid item container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h4">Products</Typography>
          </Grid>
            <CategoriesMenu/>
        </Grid>
        <Grid item>
          <Grid item container direction="row" spacing={1}>
            {fetchLoading ? (
                <Grid container justifyContent="center" alignItems="center">
                    <Grid item>
                        <CircularProgress/>
                    </Grid>
                </Grid>
            ) : products.map(product => (
              <ProductItem
                key={product._id}
                id={product._id}
                title={product.title}
                price={product.price}
                image={product.image}
              />
            ))}
          </Grid>
        </Grid>
      </Grid>
  );
};

export default Products;