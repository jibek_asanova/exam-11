import React from 'react';
import {AppBar, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import UserMenu from "./Menu/UserMenu";
import AnonymousMenu from "./Menu/AnonymousMenu";

const useStyles = makeStyles(theme => ({
  mainLink: {
    color: "inherit",
    textDecoration: 'none',
    '$:hover': {
      color: 'success'
    }
  },
  staticToolbar: {
    marginBottom: theme.spacing(2)
  }
}));

const AppToolbar = () => {
  const classes = useStyles();
  const user = useSelector(state => state.users.user);

  return (
    <>
      <AppBar position="fixed" style={{ background: '#519B0C' }}>
        <Toolbar>
          <Grid container justifyContent="space-between" alignItems="center">
            <Typography variant="h6">
              <Link to="/" className={classes.mainLink}>Lalafo</Link>
            </Typography>
            <Grid item>

            </Grid>
            <Grid item>
              {user ? (
                  <UserMenu user={user}/>
              ): (
                  <AnonymousMenu/>
              )}
            </Grid>

          </Grid>
        </Toolbar>
      </AppBar>
      <Toolbar className={classes.staticToolbar}/>
    </>
  );
};

export default AppToolbar;