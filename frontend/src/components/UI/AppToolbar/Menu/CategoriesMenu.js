import React, {useEffect, useState} from 'react';
import {Button, Menu, MenuItem} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories} from "../../../../store/actions/categoriesAction";
import {Link} from "react-router-dom";

const CategoriesMenu = () => {
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);
    const categories = useSelector(state => state.categories.categories);

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);


    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
                Categories
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem component={Link} to={'/'}>All</MenuItem>
                {categories.map(category => (
                    <MenuItem
                        component={Link}
                        key={category._id}
                        to={`/?category=${category._id}`}
                    >
                        {category.title}
                    </MenuItem>
                ))}
            </Menu>
        </>
    );
};

export default CategoriesMenu;