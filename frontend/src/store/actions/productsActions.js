import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_PRODUCTS_REQUEST = 'FETCH_PRODUCTS_REQUEST';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_FAILURE = 'FETCH_PRODUCTS_FAILURE';

export const CREATE_PRODUCT_REQUEST = 'CREATE_PRODUCT_REQUEST';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';
export const CREATE_PRODUCT_FAILURE = 'CREATE_PRODUCT_FAILURE';

export const FETCH_PRODUCT_REQUEST = 'FETCH_PRODUCT_REQUEST';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_PRODUCT_FAILURE = 'FETCH_PRODUCT_FAILURE';

export const DELETE_PRODUCT_REQUEST = 'DELETE_PRODUCT_REQUEST';
export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';
export const DELETE_PRODUCT_FAILURE = 'DELETE_PRODUCT_FAILURE';

export const fetchProductsRequest = () => ({type: FETCH_PRODUCTS_REQUEST});
export const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, payload: products});
export const fetchProductsFailure = () => ({type: FETCH_PRODUCTS_FAILURE});

export const createProductRequest = () => ({type: CREATE_PRODUCT_REQUEST});
export const createProductSuccess = () => ({type: CREATE_PRODUCT_SUCCESS});
export const createProductFailure = () => ({type: CREATE_PRODUCT_FAILURE});

export const fetchProductRequest = () => ({type: FETCH_PRODUCT_REQUEST});
export const fetchProductSuccess = product => ({type: FETCH_PRODUCT_SUCCESS, payload: product});
export const fetchProductFailure = () => ({type: FETCH_PRODUCT_FAILURE});

export const deleteProductRequest = () => ({type: DELETE_PRODUCT_REQUEST});
export const deleteProductSuccess = product => ({type: DELETE_PRODUCT_SUCCESS, payload: product});
export const deleteProductFailure = () => ({type: DELETE_PRODUCT_FAILURE});

export const fetchProducts = (query) => {
    return async dispatch => {
        try {
            dispatch(fetchProductsRequest());
            const response = await axiosApi.get('/products' + query);
            dispatch(fetchProductsSuccess(response.data));
        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(fetchProductsFailure(error.response.data));
                toast.error(`error: ${error.response.data.error}`);
            } else {
                dispatch(fetchProductsFailure({global: 'No internet'}));
                toast.error('No internet');
            }
        }
    }
};

export const createProduct = productData => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization' : getState().users.user && getState().users.user.token
            };
            dispatch(createProductRequest());
            await axiosApi.post('/products', productData, {headers});
            dispatch(createProductSuccess());
            toast.success('Product Created!');
            dispatch(historyPush('/'));
        } catch (error) {
            if(error.response.status === 401) {
                toast.warning('You need to login')
            } else if(error.response && error.response.data) {
                dispatch(createProductFailure(error.response.data));
            } else {
                dispatch(createProductFailure({global: 'No internet'}));
                toast.error('No internet');
            }
        }
    }
};

export const fetchProduct = id => {
    return async dispatch => {
        try {
            dispatch(fetchProductRequest());
            const response = await axiosApi.get('products/' + id);
            dispatch(fetchProductSuccess(response.data));
        } catch (e) {
            dispatch(fetchProductFailure());
        }
    };
};

export const deleteProduct = (id, query) => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization' : getState().users.user && getState().users.user.token
            };
            dispatch(deleteProductRequest());
            const response = await axiosApi.delete('products/' + id, {headers});
            dispatch(deleteProductSuccess(response.data));
            toast.success('Product deleted');
            dispatch(fetchProducts(query));
        } catch (error) {
            if(error.response && error.response.data) {
                dispatch(deleteProductFailure());
                toast.warning(error.response.data.error)
            } else {
                dispatch(deleteProductFailure({global: 'No internet'}));
                toast.error('No internet');
            }
        }
    }
};

