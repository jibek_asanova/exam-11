const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Category = require("./models/Category");
const Product = require("./models/Product");
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }
  const [admin, root] = await User.create({
      username: 'admin',
      password: '123',
      displayName: 'jibek',
      phoneNumber: 555751135,
      token: nanoid(),
    }, {
      username: 'root',
      password: '123',
      displayName: 'jibek',
      phoneNumber: 555751135,
      token: nanoid(),
    }
  )

  const [clothesCategory, carsCategory] = await Category.create({
    title: 'Clothes',
    description: 'Clothes Category'
  }, {
    title: 'Cars',
    description: 'Cars Category'
  });

  await Product.create({
      title: 'Honda Accord',
      price: 25000,
      description: 'Honda Accord 2019',
      category: carsCategory,
      image: 'fixtures/honda.jpg',
      user: admin
    },
    {
      title: 'Mers',
      price: 35000,
      description: 'Mercedes New ',
      category: carsCategory,
      image: 'fixtures/mers.jpg',
      user: admin
    }, {
      title: 'Dolce',
      price: 1500,
      description: 'Dress',
      category: clothesCategory,
      image: 'fixtures/dress.jpeg',
      user: root
    })


  await mongoose.connection.close();
};

run().catch(console.error)