const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Product = require('../models/Product');
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', auth, upload.single('image'), async (req, res) => {
  if (!req.body.title || !req.body.price || !req.body.category) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const productData = {
    title: req.body.title,
    price: req.body.price,
    category: req.body.category,
    user: req.user._id,
    description: req.body.description,
  };

  if (req.file) {
    productData.image = 'uploads/' + req.file.filename;
  }


  const product = new Product(productData);
  try {
    await product.save();
    res.send(product);
  } catch (error) {

    res.status(400).send(error);
  }
});

router.get('/',  async (req, res) => {
  try {
    const query = {};

    if(req.query.category) {
      query.category = req.query.category;
    }

    const products = await Product.find(query)
      .populate('category', 'title description')
      .populate('user', '_id');
    res.send(products);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const product = await Product.findById(req.params.id)
      .populate('category', 'title -_id')
      .populate('user', 'displayName phoneNumber');
    if (product) {
      res.send(product);
    } else {
      res.sendStatus(404).send({error: 'Product not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

router.delete('/:id', auth, async (req, res) => {
  const product = await Product.findById(req.params.id);
  if(!product) return res.status(404).send({error: 'Product not found'});

  if(req.user._id.toString() !== product.user._id.toString()) {
    return res.status(403).send({error: 'Permission denied'});
  }
  try {
    const product = await Product.findByIdAndDelete(req.params.id);

    if(product) {
      res.send(`product${product.title} removed`);
    } else {
      res.status(404).send({error: 'Product not found'})
    }
  } catch (e) {
    res.sendStatus(500);
  }
})

module.exports = router;